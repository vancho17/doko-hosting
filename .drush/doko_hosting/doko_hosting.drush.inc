<?php

/**
 * Implementation of hook_drush_command().
 */
function doko_hosting_drush_command() {
  // The provision-git_pull command has been replace with provision-git-pull from provision_git

  $items['provision-doko_pull'] = array(
    'description' => 'Git pull a repo.',
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH,
    'arguments' => array(
      'site' => dt('The site name.'),
      'platform' => dt('The platform name.'),
    ),
  );

  $items['provision-doko_create'] = array(
    'description' => 'Git create a repo.',
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH,
    'arguments' => array(
      'site' => dt('The site name.'),
      'platform' => dt('The platform name.'),
    ),
  );

  return $items;
}

/**
 * Implements the provision-git_clone command
 */
function drush_doko_hosting_provision_doko_create($site, $platform) {
  $site_name = str_replace('.', '_', str_replace('-', '_', $site));
  drush_log(dt('Accessing /var/aegir/platforms/' . $platform . '/sites/' . $site), 'ok');

  drush_log(dt('Executing command: curl -X POST -v -u vancho17:password_here -H "Content-Type: application/json" https://api.bitbucket.org/2.0/repositories/vancho17/' . $site_name . ' -d \'{"scm": "git", "is_private": "true", "fork_policy": "no_public_forks", "language": "php", "has_issues": "true", "has_wiki": "true"}\''), 'ok');
  drush_shell_cd_and_exec('/var/aegir/platforms/' . $platform . '/sites/' . $site, 'curl -X POST -v -u vancho17:password_here -H "Content-Type: application/json" https://api.bitbucket.org/2.0/repositories/vancho17/' . $site_name . ' -d \'{"scm": "git", "is_private": "true", "fork_policy": "no_public_forks", "language": "php", "has_issues": "true", "has_wiki": "true"}\'');

  drush_log(dt('Creating folder custom'), 'ok');
  drush_shell_exec('mkdir /var/aegir/platforms/' . $platform . '/sites/' . $site . '/modules/custom');

  drush_log(dt('Copying doko branching module'), 'ok');
  drush_shell_exec('cp -R /var/aegir/platforms/' . $platform . '/sites/all/modules/custom/doko_branch_detection /var/aegir/platforms/' . $platform . '/sites/' . $site . '/modules/custom');

  drush_log(dt('Copying gitignore file'), 'ok');
  drush_shell_exec('cp /var/aegir/.drush/doko_hosting/.gitignore /var/aegir/platforms/' . $platform . '/sites/' . $site);
  
  drush_log(dt('Initiating repository'), 'ok');
  drush_shell_cd_and_exec('/var/aegir/platforms/' . $platform . '/sites/' . $site, 'git init');
  
  drush_log(dt('Adding remote repository to git remote add origin https://vancho17@bitbucket.org/vancho17/' . $site_name . '.git'), 'ok');
  drush_shell_cd_and_exec('/var/aegir/platforms/' . $platform . '/sites/' . $site, 'git remote add origin https://vancho17@bitbucket.org/vancho17/' . $site_name . '.git');
  
  drush_log(dt('Staging changes...'), 'ok');
  drush_shell_cd_and_exec('/var/aegir/platforms/' . $platform . '/sites/' . $site, 'git add .');
  
  drush_log(dt('Committing changes...'), 'ok');
  drush_shell_cd_and_exec('/var/aegir/platforms/' . $platform . '/sites/' . $site, 'git commit -m "First commit"');
  
  drush_log(dt('Pushing repository to master...'), 'ok');
  drush_shell_cd_and_exec('/var/aegir/platforms/' . $platform . '/sites/' . $site, 'git push origin master');

  drush_log(dt('Creating dev branch...'), 'ok');
  drush_shell_cd_and_exec('/var/aegir/platforms/' . $platform . '/sites/' . $site, 'git checkout -b dev');

  drush_log(dt('Pushing repository to dev...'), 'ok');
  drush_shell_cd_and_exec('/var/aegir/platforms/' . $platform . '/sites/' . $site, 'git push origin dev');

  drush_log(dt('Checking out master...'), 'ok');
  drush_shell_cd_and_exec('/var/aegir/platforms/' . $platform . '/sites/' . $site, 'git checkout master');

  drush_log(dt('Creating staging branch...'), 'ok');
  drush_shell_cd_and_exec('/var/aegir/platforms/' . $platform . '/sites/' . $site, 'git checkout -b staging');

  drush_log(dt('Pushing repository to staging...'), 'ok');
  drush_shell_cd_and_exec('/var/aegir/platforms/' . $platform . '/sites/' . $site, 'git push origin staging');
}

/**
 * Implements the provision-git_clone command
 */
function drush_doko_hosting_provision_doko_pull($site, $platform) {
  $site_name = str_replace('.', '_', str_replace('-', '_', $site));
  drush_log(dt('Accessing /var/aegir/platforms/' . $platform . '/sites/' . $site), 'ok');

  drush_shell_cd_and_exec('/var/aegir/platforms/' . $platform . '/sites/' . $site, 'git pull origin staging');
  $output = drush_shell_exec_output();
  drush_log(implode('<br>', $output), 'ok');
}

/**
 * Map values of site node into command line arguments
 */
function drush_doko_hosting_pre_hosting_task($task) {
  $task =& drush_get_context('HOSTING_TASK');

  // Pass the dialog entries to the drush provision script
  if ($task->task_type == 'doko_pull' || $task->task_type == 'doko_create') {
    $site = node_load($task->rid);
    $task->args[1] = $site->title;
    $platform = node_load($site->platform);
    $task->args[2] = $platform->title;
  }
}